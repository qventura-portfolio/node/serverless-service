const pathToKeys = (path) => {
  return Array.isArray(path) ? path : path.split('.');;
}

export const deepGet = (obj, path) : any => {
  pathToKeys(path).forEach((key) => {
    if (!obj || !obj[key]) {
      obj = undefined;
      return;
    }

    obj = obj[key];
  });

  return obj;
}

export const deepSet = (obj, path, value) => {
  const keys = pathToKeys(path);
  const lastKey = keys.splice(-1,1)

  keys.forEach((key) => {
    if (!obj[key]) {
      obj[key] = {};
    }

    obj = obj[key];
  });

  obj[lastKey] = value;
  return value;
}

const deep = (obj, path, value) : any => {
  if (arguments.length === 3) {
    return deepSet.apply(null, arguments);
  }

  return deepGet.apply(null, arguments);
}
