import * as sinon from 'sinon';

export default class TestHelper {
  public _sinonSandbox = null;

  createSinonSandbox() {
    this._sinonSandbox = sinon.sandbox.create();
  }

  restoreSinonSandbox() {
    this._sinonSandbox.restore();
  }

  spy(...args) {
    return this._sinonSandbox.spy(...args);
  }
}
