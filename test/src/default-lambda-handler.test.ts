// import { expect } from 'chai'
// import { spy } from 'sinon'
//
// import DefaultLambdaHandler from '../../src/default-lambda-handler';
// import * as Middlewares from '../../src/middlewares';
//
// import { mockRouter, mockRestApiEvent } from '../mocks';
//
// const SERVICE_NAME = 'someServiceName';
//
// describe('DefaultLambdaHandler', () => {
//   describe('constructor', () => {
//     const useSpy = spy(DefaultLambdaHandler.prototype, 'use');
//     const customMiddleware = (ctx, next) => { next() }
//     const target = new DefaultLambdaHandler(SERVICE_NAME, mockRouter, mockRestApiEvent, {}, [customMiddleware]);
//
//     it('should call use with setContextMetadata', () => {
//       expect(useSpy.getCalls()[0].args[0].toString()).to.eql(Middlewares.setContextMetadata(SERVICE_NAME, mockRestApiEvent, {}).toString());
//     });
//
//     it('should call use with initLogger', () => {
//       expect(useSpy.getCalls()[1].args[0]).to.eql(Middlewares.initLogger);
//     });
//
//     it('should call use with setRequestFromRawLambdaEvent', () => {
//       expect(useSpy.getCalls()[2].args[0].toString()).to.eql(Middlewares.setRequestFromRawLambdaEvent(mockRestApiEvent).toString());
//     });
//
//     it('should call use with logRequest', () => {
//       expect(useSpy.getCalls()[3].args[0]).to.eql(Middlewares.logRequest);
//     });
//
//     it('should call use with errorHandler', () => {
//       expect(useSpy.getCalls()[4].args[0]).to.eql(Middlewares.errorHandler);
//     });
//
//     it('should call use with customMiddleware', () => {
//       expect(useSpy.getCalls()[5].args[0]).to.eql(customMiddleware);
//     });
//
//
//
//     it('should call use with routeResolver', () => {
//       expect(useSpy.getCalls()[6].args[0].toString()).to.eql(Middlewares.routeResolver(mockRouter).toString());
//     });
//   });
// });
