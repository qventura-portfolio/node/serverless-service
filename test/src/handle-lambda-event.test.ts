// import { expect } from 'chai'
// import { spy } from 'sinon'
//
// import {
//   ControllerRoute,
//   RestApi,
//   Router,
// } from '../../src/models';
//
// import DefaultLambdaHandler from '../../src/default-lambda-handler';
// import handleLambdaEvent from '../../src/handle-lambda-event';
//
// import mockEvent from '../mocks/mock-rest-api-event';
//
// const responseStatusCode = 200;
// const responseBody = { user: { username: 'user123' }};
//
// describe('handleLambdaEvent', () => {
//   const lambdaHandlerRunSpy = spy(DefaultLambdaHandler.prototype, 'run');
//
//   describe('when there is no error during execution', () => {
//     const callbackSpy = spy();
//
//     const mockRouter = new Router([
//       new ControllerRoute(
//         new RestApi.Route(
//           RestApi.HttpMethods.GET,
//           'users/:id'
//         ),
//         spy((ctx : RestApi.Context, next) => {
//           ctx.response.statusCode = responseStatusCode;
//           ctx.response.body = responseBody;
//
//           next();
//         })
//       )
//     ]);
//
//     before((done) => {
//       handleLambdaEvent('someServiceName', mockRouter, mockEvent, {}, callbackSpy)
//         .then((result) => {
//           done();
//         }).catch((error) => {
//           throw error;
//         })
//     })
//
//     after(() => {
//       lambdaHandlerRunSpy.restore();
//     })
//
//     it('should call DefaultLambdaHandler.run', () => {
//       expect(lambdaHandlerRunSpy).to.have.been.calledOnce;
//     })
//
//     it('should call the route resolver middleware ', () => {
//       expect(mockRouter.controllerRoutes[0].resolver).to.have.been.called;
//     });
//
//     it('should call the callback with null as the error parameter', () => {
//       expect(callbackSpy.getCall(0).args[0]).to.not.exist;
//     });
//
//     it('should call the callback with non-null as the response parameter', () => {
//       expect(callbackSpy.getCall(0).args[1]).to.exist;
//     });
//
//     it('should set the statusCode of the response parameter', () => {
//       expect(callbackSpy.getCall(0).args[1].statusCode).to.exist;
//       expect(callbackSpy.getCall(0).args[1].statusCode).to.eql(responseStatusCode);
//     });
//
//     it('should set the body of the response parameter as a string', () => {
//       expect(callbackSpy.getCall(0).args[1].body).to.exist;
//       expect(typeof callbackSpy.getCall(0).args[1].body).to.eql('string');
//       expect(callbackSpy.getCall(0).args[1].body).to.eql(JSON.stringify(responseBody));
//     });
//   })
//
//   describe('when one of the middleware throw an error', () => {
//     const callbackSpy = spy();
//
//     const mockRouter = new Router([
//       new ControllerRoute(
//         new RestApi.Route(
//           RestApi.HttpMethods.GET,
//           'userss/:id'
//         ),
//         spy((ctx : RestApi.Context, next) => {
//           throw Error('Some error message ...');
//         })
//       )
//     ]);
//
//     before((done) => {
//       handleLambdaEvent('someServiceName', mockRouter, mockEvent, {}, callbackSpy)
//         .then((result) => {
//           done();
//         }).catch((error) => {
//           throw error;
//         })
//     })
//
//     after(() => {
//       lambdaHandlerRunSpy.restore();
//     })
//
//     it('should call DefaultLambdaHandler.run', () => {
//       expect(lambdaHandlerRunSpy).to.have.been.calledOnce;
//     })
//
//     it('should call the callback with an error parameter', () => {
//       expect(callbackSpy.getCall(0).args[0]).to.exist;
//     });
//
//     it('should call the callback with an empty string as the response parameter', () => {
//       expect(callbackSpy.getCall(0).args[1]).to.be.null;
//     });
//   })
// });
