import { expect } from 'chai'
import { spy } from 'sinon'

import { LambdaError } from '../../../src/errors';

import {
  ControllerRoute,
  Kinesis,
  Router,
} from '../../../src';

const { ErrorResponse, handleLambdaEvent } = Kinesis;

import { MockKinesisLambdaArgsFactory } from '../../mocks';

const callbackSpy = spy();
const firstEventId = 'eventId1';
const secondEventId = 'eventId2';
const failingRequestId = 'failingRequestId123';

const lambdaArgs = MockKinesisLambdaArgsFactory.create({
  streamName: 'userCreated',
  callback: callbackSpy,
  records: [{
      eventId: firstEventId,
    }, {
      eventId: secondEventId,
      consolidatedRequestId: failingRequestId,
    }
  ]
});

const responseBody = [ { user: { username: 'user123' }} ];

describe('handleLambdaEvent', () => {
  describe('when there is no error during execution', () => {
    const responseStatusCode = 200;

    const expectedResponse = {
        errors: [],
        data: [{
        eventId: firstEventId,
        statusCode: responseStatusCode,
        body: responseBody,
      }, {
        eventId: secondEventId,
        statusCode: responseStatusCode,
        body: responseBody,
        }
      ]
    }

    const router : Router = new Router({ controllerRoutes: [
      new ControllerRoute({
        route: new Kinesis.Route({
          streamName: 'userCreated',
        }),
        resolver: spy((ctx : Kinesis.Context, next) => {
          ctx.response.statusCode = responseStatusCode;
          ctx.response.body = responseBody;

          next();
        })
      })
    ]});

    before((done) => {
      handleLambdaEvent({
        serviceName: 'someServiceName',
        router: router,
        rawLambdaEvent: lambdaArgs.event,
        rawLambdaContext: lambdaArgs.context,
        customMiddlewares: [],
        callback: lambdaArgs.callback
      })
      .then((result) => {
        done();
      }).catch((error) => {
        throw error;
      })
    })

    after(() => {
      callbackSpy.reset();
    })

    it('should call the callback', () => {
      expect(callbackSpy).to.have.been.calledOnce;
    })

    it('should call the callback with null as the error parameter', () => {
      expect(callbackSpy.getCall(0).args[0]).to.not.exist;
    });

    it('should call the callback with an array as the response parameter', () => {
      expect(callbackSpy.getCall(0).args[1]).to.exist;
    });

    it('should set the statusCode of the response parameter', () => {
      expect(callbackSpy.getCall(0).args[1].statusCode).to.exist;
      expect(callbackSpy.getCall(0).args[1].statusCode).to.eql(responseStatusCode);
    });

    it('should set the body of the response parameter as a string', () => {
      expect(callbackSpy.getCall(0).args[1].body).to.exist;
      expect(typeof callbackSpy.getCall(0).args[1].body).to.eql('string');

      const actualResult = JSON.parse(callbackSpy.getCall(0).args[1].body);
      expect(actualResult).to.eql(expectedResponse);
    });
  })

  describe('when one of the event cause a LambdaError to be thrown', () => {
    const responseStatusCode = 200;
    const errorStatusCode = 400;
    const lambdaError = new LambdaError({ statusCode: errorStatusCode, errorCode: 1000, errorMessage: 'someError' })

    const expectedResponse = {
      errors: [ new ErrorResponse({
        eventId: secondEventId,
        error: lambdaError
      })],
      data: [{
        eventId: firstEventId,
        statusCode: responseStatusCode,
        body: responseBody,
      }]
    }

    const router : Router = new Router({ controllerRoutes: [
      new ControllerRoute({
        route: new Kinesis.Route({
          streamName: 'userCreated',
        }),
        resolver: spy((ctx : Kinesis.Context, next) => {
          if(ctx.request.headers.consolidatedRequestId == failingRequestId) {
            throw lambdaError;
          }

          ctx.response.statusCode = responseStatusCode;
          ctx.response.body = responseBody;

          next();
        })
      })
    ]});

    before((done) => {
      handleLambdaEvent({
        serviceName: 'someServiceName',
        router: router,
        rawLambdaEvent: lambdaArgs.event,
        rawLambdaContext: lambdaArgs.context,
        customMiddlewares: [],
        callback: lambdaArgs.callback
      })
      .then((result) => {
        done();
      }).catch((error) => {
        throw error;
      })
    })

    after(() => {
      callbackSpy.reset();
    })

    it('should call the callback', () => {
      expect(callbackSpy).to.have.been.calledOnce;
    })

    it('should call the callback with null as the error parameter', () => {
      expect(callbackSpy.getCall(0).args[0]).to.not.exist;
    });

    it('should call the callback with an array as the response parameter', () => {
      expect(callbackSpy.getCall(0).args[1]).to.exist;
    });

    it('should set the statusCode of the response parameter', () => {
      expect(callbackSpy.getCall(0).args[1].statusCode).to.exist;
      expect(callbackSpy.getCall(0).args[1].statusCode).to.eql(errorStatusCode);
    });

    it('should set the body of the response parameter as a string', () => {
      expect(callbackSpy.getCall(0).args[1].body).to.exist;
      expect(typeof callbackSpy.getCall(0).args[1].body).to.eql('string');

      const actualResult = JSON.parse(callbackSpy.getCall(0).args[1].body);
      expect(actualResult).to.eql(expectedResponse);
    });
  })

  describe('when one of the event cause a unhandled error to be thrown', () => {
    const responseStatusCode = 200;
    const error = new Error('someError');

    const expectedResponse = {
      errors: [ new ErrorResponse({
        eventId: secondEventId,
        error: error
      })],
      data: [{
        eventId: firstEventId,
        statusCode: responseStatusCode,
        body: responseBody,
      }]
    }

    const router : Router = new Router({ controllerRoutes: [
      new ControllerRoute({
        route: new Kinesis.Route({
          streamName: 'userCreated',
        }),
        resolver: spy((ctx : Kinesis.Context, next) => {
          if(ctx.request.headers.consolidatedRequestId == failingRequestId) {
            throw error;
          }

          ctx.response.statusCode = responseStatusCode;
          ctx.response.body = responseBody;

          next();

        })
      })
    ]});

    before((done) => {
      handleLambdaEvent({
        serviceName: 'someServiceName',
        router: router,
        rawLambdaEvent: lambdaArgs.event,
        rawLambdaContext: lambdaArgs.context,
        customMiddlewares: [],
        callback: lambdaArgs.callback
      })
      .then((result) => {
        done();
      }).catch((error) => {
        done();
      })
    })

    after(() => {
      callbackSpy.reset();
    })

    it('should call the callback', () => {
      expect(callbackSpy).to.have.been.calledOnce;
    })

    it('should call the callback an array of responses', () => {
      expect(callbackSpy.getCall(0).args[0].body).to.exist;
      expect(typeof callbackSpy.getCall(0).args[0].body).to.eql('string');

      const actualResult = JSON.parse(callbackSpy.getCall(0).args[0].body);
      expect(actualResult).to.eql(expectedResponse);
    });

    it('should call the callback with undefined as the response parameter', () => {
      expect(callbackSpy.getCall(0).args[1]).to.be.undefined;
    });
  })
});
