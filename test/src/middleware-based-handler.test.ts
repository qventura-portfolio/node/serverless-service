// import { expect } from 'chai'
// import { spy } from 'sinon'
//
// import MiddlewareBasedHandler from '../../src/middleware-based-handler';
//
// describe('MiddlewareBasedHandler', () => {
//   const ctx = {};
//   let target;
//   let firstMiddlewareSpy;
//   let secondMiddlewareSpy;
//
//   beforeEach(() => {
//     target = new MiddlewareBasedHandler();
//     firstMiddlewareSpy = spy(({}, next) => { next(); });
//     secondMiddlewareSpy = spy(({}, next) => { next(); });
//
//     target.use(firstMiddlewareSpy);
//     target.use(secondMiddlewareSpy);
//   });
//
//   describe('run', () => {
//     it('should call the compose method with the middlewares as parameters', () => {
//       target.run(ctx)
//
//       expect(firstMiddlewareSpy).to.have.been.calledOnce;
//       expect(secondMiddlewareSpy).to.have.been.calledOnce;
//     });
//   });
//
//   describe('use', () => {
//     it('should add the middleware parameter to _middlewares', () => {
//       const thirdMiddlewareSpy = spy();
//
//       target.use(thirdMiddlewareSpy)
//
//       expect(target._middlewares[2]).to.eql(thirdMiddlewareSpy);
//     });
//   });
//
//   describe('_getMiddlewares', () => {
//     it('should return the content of _middleware', () => {
//       expect(target._getMiddlewares()[0]).to.eql(firstMiddlewareSpy);
//       expect(target._getMiddlewares()[1]).to.eql(secondMiddlewareSpy);
//     });
//   });
// });
