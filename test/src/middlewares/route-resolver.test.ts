import { expect } from 'chai'
import { spy } from 'sinon'

import { RouteNotFoundError } from '../../../src/errors';
import { routeResolver } from '../../../src/middlewares';

import { ControllerRoute, Logger, RestApi, Router } from '../../../src';
const { Context, HttpMethods, Request, Route } = RestApi;

describe('routeResolver', () => {
  const resolverSpy = spy((ctx, next) => { next(); });
  const nextSpy = spy(() => { });

  const router = new Router({
    controllerRoutes: [ new ControllerRoute({
      route: new Route({
        httpMethod: HttpMethods.GET,
        path: 'resource'
      }),
      resolver: resolverSpy
    })
  ]});

  describe('when the route exists', () => {
    const ctx : RestApi.Context = new Context({
      request: new Request({
        route: new Route({
          httpMethod: HttpMethods.GET,
          path: 'resource'
        }
      )})
    });

    before((done) => {
      routeResolver(router)(ctx, nextSpy)
        .then((result) => {
          done();
        })
    })

    after(() => {
      resolverSpy.reset();
      nextSpy.reset();
    })

    it('should call next', () => {
      expect(nextSpy).to.have.been.calledOnce;
    });

    it('should call the resolver', () => {
      expect(resolverSpy).to.have.been.calledOnce;
    });
  })

  describe('when the route exists', () => {
    let thrownError;

    const ctx : RestApi.Context = new Context({
      request: new Request({
        route: new Route({
          httpMethod: HttpMethods.GET,
          path: 'other_resource'
        }
      )})
    });

    before((done) => {
      routeResolver(router)(ctx, nextSpy)
        .catch((error) => {
          thrownError = error;
          done();
        })
    })

    after(() => {
      resolverSpy.reset();
      nextSpy.reset();
    })

    it('should throw a RouteNotFoundError', () => {
      expect(thrownError).to.be.an.instanceOf(RouteNotFoundError);
    });

    it('should not call next', () => {
      expect(nextSpy).to.not.have.been.called;
    });

    it('should not call the resolver', () => {
      expect(resolverSpy).to.not.have.been.called;
    });
  })

});
