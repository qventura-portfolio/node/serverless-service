import { expect } from 'chai'
import { spy } from 'sinon'

import { errorHandler } from '../../../src/middlewares';

import { RestApi } from '../../../src';
import { LambdaError } from '../../../src/errors';
import { Logger } from '../../../src';

describe('errorHandler', () => {
  const ctx : RestApi.Context = new RestApi.Context();
  const loggerSpy = spy(ctx.getLogger(), 'error');

  describe('when an error is thrown', () => {
    const error = new Error('Some message');
    const expectedErrorBody = {
      error: {
        message: error.message,
        errorCode: undefined,
        statusCode: undefined
      }
    }

    const nextSpy = spy((ctx, next) => { throw error });
    let thrownError;

    before((done) => {
      errorHandler(ctx, nextSpy)
        .catch((result) => {
          thrownError = result;
          done();
        })
    })

    after(() => {
      nextSpy.reset();
    })

    it('should set the reponse body with the error', () => {
      expect(ctx.response.body).to.eql(expectedErrorBody);
    })

    it('should set 5600 as the reponse statusCode', () => {
      expect(ctx.response.statusCode).to.eql(500);
    })

    it('should log the error', () => {
      expect(loggerSpy).to.have.been.calledOnce;
    })

    it('should throw it back', () => {
      expect(thrownError).to.be.instanceof(Error);
    })

    it('should call next', () => {
      expect(nextSpy).to.have.been.calledOnce;
    });
  })

  describe('when a LambdaError is thrown', () => {
    const error = new LambdaError({ errorMessage: 'Some message', statusCode: 500, errorCode: 1000 });

    console.log(error);

    const expectedErrorBody = { error: {
        message: error.message,
        errorCode: error.errorCode,
        statusCode: error.statusCode,
      }
    }

    const nextSpy = spy((ctx, next) => { throw error });
    let thrownError;

    before((done) => {
      errorHandler(ctx, nextSpy)
        .catch((result) => {
          thrownError = result;
          done();
        })
    })

    after(() => {
      nextSpy.reset();
    })

    it('should set the reponse body with the error', () => {
      expect(ctx.response.body).to.eql(expectedErrorBody);
    })

    it('should set the reponse statusCode with the error statusCode', () => {
      expect(ctx.response.statusCode).to.eql(error.statusCode);
    })
  })
});
