import { expect } from 'chai'
import { spy } from 'sinon'

import { logRequest } from '../../../src/middlewares';
import { RestApi } from '../../../src';

describe('logRequest', () => {
  const ctx : RestApi.Context = new RestApi.Context();
  const loggerInfoSpy = spy(ctx.getLogger(), 'info');
  const nextSpy = spy(() => { });

  before((done) => {
    logRequest(ctx, nextSpy)
      .then(() => {
        done();
      })
  })

  after(() => {
    ctx.getLogger().info.restore();
  })

  it('should call Logger.info', () => {
    expect(loggerInfoSpy).to.have.been.calledTwice;
  })

  it('should call next', () => {
    expect(nextSpy).to.have.been.calledOnce;
  });
});
