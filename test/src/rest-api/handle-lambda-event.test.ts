import { expect } from 'chai'
import { spy } from 'sinon'

import { LambdaError } from '../../../src/errors';

import {
  ControllerRoute,
  RestApi,
  Router,
} from '../../../src';

const { ErrorResponse, handleLambdaEvent } = RestApi;

import { MockRestApiLambdaArgsFactory } from '../../mocks';

const callbackSpy = spy();

const lambdaArgs = MockRestApiLambdaArgsFactory.create({
  httpMethod: RestApi.HttpMethods.GET,
  path: 'users/123',
  callback: callbackSpy
});

const responseStatusCode = 200;
const responseBody = { user: { username: 'user123' }};

describe('handleLambdaEvent', () => {
  describe('when there is no error during execution', () => {

    const router : Router = new Router({ controllerRoutes: [
      new ControllerRoute({
        route: new RestApi.Route({
          httpMethod: RestApi.HttpMethods.GET,
          path: 'users/:id'
        }),
        resolver: spy((ctx : RestApi.Context, next) => {
          ctx.response.statusCode = responseStatusCode;
          ctx.response.body = responseBody;

          next();
        })
      })
    ]});

    const expectedResponse = { data: responseBody };

    before((done) => {
      handleLambdaEvent({
        serviceName: 'someServiceName',
        router: router,
        rawLambdaEvent: lambdaArgs.event,
        rawLambdaContext: lambdaArgs.context,
        customMiddlewares: [],
        callback: lambdaArgs.callback
      })
      .then((result) => {
        done();
      }).catch((error) => {
        throw error;
      })
    })

    after(() => {
      callbackSpy.reset();
    })

    it('should call the callback', () => {
      expect(callbackSpy).to.have.been.calledOnce;
    })

    it('should call the callback with null as the error parameter', () => {
      expect(callbackSpy.getCall(0).args[0]).to.not.exist;
    });

    it('should call the callback with non-null as the response parameter', () => {
      expect(callbackSpy.getCall(0).args[1]).to.exist;
    });

    it('should set the statusCode of the response parameter', () => {
      expect(callbackSpy.getCall(0).args[1].statusCode).to.exist;
      expect(callbackSpy.getCall(0).args[1].statusCode).to.eql(responseStatusCode);
    });

    it('should set the body of the response parameter as a string', () => {
      expect(callbackSpy.getCall(0).args[1].body).to.exist;
      expect(typeof callbackSpy.getCall(0).args[1].body).to.eql('string');

      const actualResult = JSON.parse(callbackSpy.getCall(0).args[1].body);
      expect(actualResult).to.eql(expectedResponse);
    });
  })

  describe('when one of the middleware throw an LambdaError', () => {
    const errorStatusCode = 400;
    const lambdaError = new LambdaError({ statusCode: errorStatusCode, errorCode: 1000, errorMessage: 'someError' })

    const router : Router = new Router({ controllerRoutes: [
      new ControllerRoute({
        route: new RestApi.Route({
          httpMethod: RestApi.HttpMethods.GET,
          path: 'users/:id'
        }),
        resolver: spy((ctx : RestApi.Context, next) => {
          throw lambdaError;
        })
      })
    ]});

    const expectedResponse = new ErrorResponse({ error: lambdaError }).body

    before((done) => {
      handleLambdaEvent({
        serviceName: 'someServiceName',
        router: router,
        rawLambdaEvent: lambdaArgs.event,
        rawLambdaContext: lambdaArgs.context,
        customMiddlewares: [],
        callback: lambdaArgs.callback
      })
      .then((result) => {
        done();
      }).catch((error) => {
        done();
      })
    })

    after(() => {
      callbackSpy.reset();
    })

    it('should call the callback', () => {
      expect(callbackSpy).to.have.been.calledOnce;
    })

    it('should call the callback with null as the error parameter', () => {
      expect(callbackSpy.getCall(0).args[0]).to.not.exist;
    });

    it('should call the callback with non-null as the response parameter', () => {
      expect(callbackSpy.getCall(0).args[1]).to.exist;
    });

    it('should set the statusCode of the response parameter', () => {
      expect(callbackSpy.getCall(0).args[1].statusCode).to.exist;
      expect(callbackSpy.getCall(0).args[1].statusCode).to.eql(lambdaError.statusCode);
    });

    it('should set the body of the response parameter as a string', () => {
      expect(callbackSpy.getCall(0).args[1].body).to.exist;
      expect(typeof callbackSpy.getCall(0).args[1].body).to.eql('string');

      const actualResult = JSON.parse(callbackSpy.getCall(0).args[1].body);
      expect(actualResult).to.eql(expectedResponse);
    });
  });

  describe('when one of the middleware throw an unhandled error', () => {
    const error = new Error('someError');

    const router : Router = new Router({ controllerRoutes: [
      new ControllerRoute({
        route: new RestApi.Route({
          httpMethod: RestApi.HttpMethods.GET,
          path: 'users/:id'
        }),
        resolver: spy((ctx : RestApi.Context, next) => {
          throw error;
        })
      })
    ]});

    const expectedResponse = new ErrorResponse({ error: error }).body

    before((done) => {
      handleLambdaEvent({
        serviceName: 'someServiceName',
        router: router,
        rawLambdaEvent: lambdaArgs.event,
        rawLambdaContext: lambdaArgs.context,
        customMiddlewares: [],
        callback: lambdaArgs.callback
      })
      .then((result) => {
        done();
      }).catch((error) => {
        done();
      })
    })

    after(() => {
      callbackSpy.reset();
    })

    it('should call the callback', () => {
      expect(callbackSpy).to.have.been.calledOnce;
    })

    it('should call the callback an array of responses', () => {
      expect(callbackSpy.getCall(0).args[0].body).to.exist;
      expect(typeof callbackSpy.getCall(0).args[0].body).to.eql('string');

      const actualResult = JSON.parse(callbackSpy.getCall(0).args[0].body);
      expect(actualResult).to.eql(expectedResponse);
    });

    it('should call the callback with undefined as the response parameter', () => {
      expect(callbackSpy.getCall(0).args[1]).to.be.undefined;
    });
  })
});
