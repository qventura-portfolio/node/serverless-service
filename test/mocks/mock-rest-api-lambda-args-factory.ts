export const MockRestApiLambdaArgsFactory =  {
  create({
    httpMethod,
    path,
    basePath = '/some-service/v1',
    apiKey = 'apiKey123',
    consolidatedRequestId = `consolidatedRequestId${Math.floor(Math.random() * 10000000000)}`,
    requestId = `lambdaRequestId${Math.floor(Math.random() * 10000000000)}`,
    environment = 'test',
    body = null,
    isBase64Encoded = false,
    callback = () => {}
  }) {
    return {
      event: {
        resource:`${basePath}/{proxy+}`,
        path:`${basePath}/${path}`,
        httpMethod: httpMethod,
        headers: {
          "Accept": "*/*",
          "Accept-Encoding": "gzip, deflate",
          "cache-control": "no-cache",
          "CloudFront-Forwarded-Proto": "https",
          "CloudFront-Is-Desktop-Viewer": "true",
          "CloudFront-Is-Mobile-Viewer": "false",
          "CloudFront-Is-SmartTV-Viewer": "false",
          "CloudFront-Is-Tablet-Viewer": "false",
          "CloudFront-Viewer-Country": "CA",
          "Host": "somehostId.execute-api.ca-central-1.amazonaws.com",
          "User-Agent": "someUserAgent",
          "Via": "1.1 someId123.cloudfront.net (CloudFront)",
          "X-Amz-Cf-Id": "someAmzCfId123",
          "X-Amzn-Trace-Id": "Root=AmznTraceId123",
          "X-Api-Key": "apiKey123",
          "X-Forwarded-For": "1xx.xxx.xxx.xxx, xxx.xxx.xxxxxx",
          "X-Forwarded-Port": "443",
          "X-Forwarded-Proto": "https",
          "X-Request-Id": consolidatedRequestId
        },
        queryStringParameters:null,
        pathParameters:{
          proxy: path
        },
        requestContext:{
          path: `${basePath}/{proxy+}`,
          accountId: "awsAccountId123",
          resourceId: "apiGatewayResourceId",
          stage: environment,
          requestId: requestId,
          identity:{
            cognitoIdentityPoolId: null,
            cognitoIdentityId: null,
            apiKey:"someCognitoApiKey",
            cognitoAuthenticationType:null,
            userArn: "arn:aws:iam::awsAccountId123:user/someUsername",
            apiKeyId: "someCognitoApiKeyId",
            userAgent: "Apache-HttpClient/4.5.x (Java/1.8.0_144)",
            accountId: "awsAccountId123",
            caller: "callerAccessKey",
            sourceIp: "someSourceId",
            accessKey: "someAccesKey",
            cognitoAuthenticationProvider:null,
            user: "callerAccessKey"
          },
          resourcePath: `${basePath}/{proxy+}`,
          httpMethod: httpMethod,
          apiId: "apiGatewayId"
        },
        body: body,
        isBase64Encoded: isBase64Encoded
      },
      context: {
        awsRequestId: requestId
      },
      callback: callback
    }
  }
}
