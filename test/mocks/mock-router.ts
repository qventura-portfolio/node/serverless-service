import {
  RestApi,
  ControllerRoute,
  Router,
} from '../../src';

import { spy } from 'sinon'

export const mockRouter = new Router({
  controllerRoutes: [
    new ControllerRoute({
      route: new RestApi.Route({
        httpMethod: RestApi.HttpMethods.GET,
        path: '/v1/users'
      }),
      resolver: spy()

    }),
    new ControllerRoute({
      route: new RestApi.Route({
        httpMethod: RestApi.HttpMethods.POST,
        path:'/v1/users'
      }),
      resolver: spy()
    })
  ]
});
