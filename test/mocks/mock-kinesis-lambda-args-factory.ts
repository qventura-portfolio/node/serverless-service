import { toBase64 } from '../helpers/to-base-64';

export interface RecordSchema {
  someSequenceNumber?: string;
  eventId?: string;
  apiKey?: string;
  consolidatedRequestId?: string;
  environment?: string;
  body?: any;
}

export const MockKinesisLambdaArgsFactory =  {
  create(args: { streamName: string, requestId?: string, callback: Function, records: [RecordSchema] }) {
    const requestId = args.requestId || 'requestId123';
    const callback = args.callback || function() {};
    const streamName = args.streamName;

    return {
      event: {
        Records: args.records.map((record) => {
          const someSequenceNumber = record.someSequenceNumber || 'someSequenceNumber';
          const eventId = record.eventId ||  `shardId-000000000000:${someSequenceNumber}`;
          const apiKey = record.apiKey ||  'apiKey123';
          const consolidatedRequestId = record.consolidatedRequestId ||  `consolidatedRequestId${Math.floor(Math.random() * 10000000000)}`;
          const environment = record.environment ||  'test';
          const body = record.body || null;

          const data = toBase64({
            headers: {
              apiKey: apiKey,
              consolidatedRequestId: consolidatedRequestId,
              environment: environment,
              lambdaRequestId: requestId
            },
            body: body
          })

          return {
            kinesis:{
              kinesisSchemaVersion: "1.0",
              partitionKey: "b9b7d42f-a3f7-47b6-93b5-f4c8d25f22e4",
              sequenceNumber: "someSequenceNumber",
              data: data,
              approximateArrivalTimestamp: Date.now()
            },
            eventSource: "aws:kinesis",
            eventVersion: "1.0",
            eventID: eventId,
            eventName: "aws:kinesis:record",
            invokeIdentityArn: "arn:aws:iam::awsAccountId123:role/some-api-lambda-role",
            awsRegion: "ca-central-1",
            eventSourceARN: `arn:aws:kinesis:ca-central-1:awsAccountId123:stream/${streamName}`
          }
        })
      },
      context: {
        awsRequestId: requestId
      },
      callback: callback
    }
  }
};
