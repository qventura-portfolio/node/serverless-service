import { BaseRequest, IRoute } from '..';
import { Route } from '.';

import {
  MissingRequiredPathParameterError,
} from '../errors';

const BASIC_AUTH_REGEX = /Basic (.*)/;
const HEADER_AUTHORIZATION_KEY = 'Authorization';

export interface RequestSchema {
  body?: any;
  headers?: any;
  route: Route;
}

export class Request extends BaseRequest {
  public pathArguments: any;

  constructor(constructorAttributes: RequestSchema) {
    super(constructorAttributes);
  }

  public extractUsernameAndPasswordFromBasicAuth() {
    const authorization = super.headers[HEADER_AUTHORIZATION_KEY];
    const basicAuthMatch = BASIC_AUTH_REGEX.exec(authorization);
    const basicAuth = new Buffer(basicAuthMatch[1], 'base64').toString('ascii');

    return basicAuth.split(':');
  }

  public getPathParameter(key: string, options : any = {}) : string {
    const pathParameter = this.pathArguments[key];

    if(!pathParameter && options.required == true) {
      throw new MissingRequiredPathParameterError(key)
    }

    return pathParameter;
  }

  public beforeResolvingRoute(route: IRoute) : void {
    if(!(super.route instanceof Route) || !(route instanceof Route)) {
      return;
    }

    this.pathArguments = route.getRouteParameters(super.route.path);
  }
}
