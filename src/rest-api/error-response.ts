import { LambdaError } from '../errors'

import { Response } from './response';

export interface ErrorResponseSchema {
  error: Error;
}

export class ErrorResponse extends Response {
  constructor(constructorAttributes: ErrorResponseSchema) {
    if(constructorAttributes.error instanceof LambdaError) {
      super({
        statusCode: constructorAttributes.error.statusCode,
        body: {
          errorCode: constructorAttributes.error.errorCode,
          message: constructorAttributes.error.message
        }
      });
    } else {
      super({
        statusCode: 500,
        body: {
          errorCode: 1000,
          message: constructorAttributes.error.toString()
        }
      });
    }
  }
};
