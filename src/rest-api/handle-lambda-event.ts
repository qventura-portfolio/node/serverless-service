import { LambdaError } from '../errors';
import { IMiddleware } from '../middlewares';
import { IHandleLambdaEvent, Router, runMiddlewaresStack } from '..';
import { ErrorResponse } from '.';

export const handleLambdaEvent : IHandleLambdaEvent = async (args : { serviceName: string, router: Router, rawLambdaEvent: any, rawLambdaContext: any, callback: Function, customMiddlewares: IMiddleware[] }) : Promise<void> => {
  const { serviceName, router, rawLambdaEvent, rawLambdaContext, callback, customMiddlewares } = args;

  try {
    const response = await runMiddlewaresStack({
      serviceName,
      router,
      rawLambdaEvent,
      rawLambdaContext,
      customMiddlewares
    });

    callback(null, {
      statusCode: response.statusCode,
      body: JSON.stringify({ data: response.body })
    })
  } catch(error) {
    const errorResponse = new ErrorResponse({ error: error });

    if(error instanceof LambdaError) {
      callback(null, {
        statusCode: errorResponse.statusCode,
        body: JSON.stringify(errorResponse.body)
      })
    } else {
      callback({
        statusCode: errorResponse.statusCode,
        body: JSON.stringify(errorResponse.body)
      });
    }
  }
}
