import { BaseFactory, ConfigManager, IFactory, Logger, Metadata } from '..';
import { Context, Request, Response, Route } from '.'

const HEADER_API_KEY_KEY = 'X-Api-Key';
const HEADER_SOURCE_SERVICE_KEY = 'X-Source-Service';
const HEADER_CONSOLIDATED_REQUEST_ID_KEY = 'X-Request-Id';

export interface FactorySchema {
  serviceName: string;
  rawLambdaEvent: any;
  rawLambdaContext: any;
}

export class Factory extends BaseFactory implements Partial<IFactory> {
  constructor(constructorAttributes: FactorySchema) {
    super(constructorAttributes)

    this.headers = this.rawLambdaEvent.headers || {};
    this.apiKey = this.headers[HEADER_API_KEY_KEY];
    this.consolidatedRequestId = this.headers[HEADER_CONSOLIDATED_REQUEST_ID_KEY];
    this.sourceServiceName = this.headers[HEADER_SOURCE_SERVICE_KEY];

    this.requestContext =  this.rawLambdaEvent.requestContext || {};
    this.environment = this.requestContext.stage;
    this.lambdaRequestId = this.requestContext.requestId

    const path = this.rawLambdaEvent.pathParameters.proxy;
    const httpMethod = this.rawLambdaEvent.httpMethod;

    this.body = JSON.parse(this.rawLambdaEvent.body);

    const headers = this.rawLambdaEvent.headers;
    const body = JSON.parse(this.rawLambdaEvent.body);
    this.route = new Route({ httpMethod, path });
  }

  public createContext() : Context {
    const context = new Context({
      metadata: this.createMetadata(),
      request: this.createRequest(),
      response: this.createResponse(),
      logger: new Logger(this.createLoggingContext()),
      configManager: new ConfigManager({ serviceName: this.serviceName, environment: this.environment })
    });

    return context;
  }

  private createRequest() : Request {
    return new Request({
      route: this.route,
      headers: this.headers,
      body: this.body
    });
  }

  private createResponse() : Response {
    return new Response();
  }
}
