import * as pathToRegexp from 'path-to-regexp';

import { IContext, IRoute } from '..';

import { HttpMethods, Context } from '.';

export interface RouteSchema {
  httpMethod: HttpMethods;
  path: string;
}

export class Route implements IRoute {
  public httpMethod: HttpMethods;
  public path: string;

  constructor(constructorAttributes: RouteSchema) {
    Object.assign(this, constructorAttributes);
  }

  equals(o: IRoute) {
    if(!(o instanceof Route)) {
      return false;
    }

    const sameHttpMethod = this.httpMethod.toString().toLowerCase() === o.httpMethod.toString().toLowerCase();

    if(!sameHttpMethod) {
      return false;
    }

    const matchingPath = o.path.match(pathToRegexp(this.path));

    return matchingPath != null;
  }

  public getRouteParameters(requestPath: string) {
    pathToRegexp.parse(this.path);
    const matches = pathToRegexp(this.path).exec(requestPath);
    const args = matches.slice(1).map(Route._decode);
    const tokens = pathToRegexp.parse(this.path);
    tokens.shift();

    const parsedArguments = {};

    tokens.forEach((token: any, index: number) => {
      parsedArguments[token.name] = args[index];
    })

    return parsedArguments;
  }

  public toString() : string {
    return `RestApi.Route { ${this.httpMethod} ${this.path} }`;
  }

  private static _decode(val) {
    if (val) return decodeURIComponent(val);
  }
}
