import { BaseContextSchema } from '../base-context';
import { BaseContext, IResponse } from '..';
import { Response } from '.';

export class Context extends BaseContext {
  constructor(constructorAttributes: BaseContextSchema = {}) {
    super(constructorAttributes);

    this.response = this.response || new Response();
  }
};
