import { LambdaError } from '../errors'

import { Response } from './response';

export interface ErrorResponseSchema {
  error: Error;
  eventId: string;
}

export class ErrorResponse extends Response {
  constructor(constructorAttributes: ErrorResponseSchema) {
    if(constructorAttributes.error instanceof LambdaError) {
      super({
        eventId: constructorAttributes.eventId,
        statusCode: constructorAttributes.error.statusCode,
        body: {
          errorCode: constructorAttributes.error.errorCode,
          message: constructorAttributes.error.message
        }
      });
    } else {
      super({
        eventId: constructorAttributes.eventId,
        statusCode: 500,
        body:  {
          errorCode: 1000,
          message: constructorAttributes.error.toString()
        }
      });
    }
  }
};
