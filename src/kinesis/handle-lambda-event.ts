import { LambdaError } from '../errors';
import { IMiddleware } from '../middlewares';
import { IHandleLambdaEvent, Router, runMiddlewaresStack } from '..';
import { ErrorResponse } from '.';

export const handleLambdaEvent : IHandleLambdaEvent = async (args : { serviceName: string, router: Router, rawLambdaEvent: any, rawLambdaContext: any, callback: Function, customMiddlewares: IMiddleware[] }) : Promise<void> => {
  const { serviceName, router, rawLambdaEvent, rawLambdaContext, callback, customMiddlewares } = args;
  const errors = [];
  const responses = [];
  let receivedUnhandledError = false;

  for (var i = 0, len = rawLambdaEvent['Records'].length; i < len; i++) {
    const record = rawLambdaEvent['Records'][i];

    try {
      const response = await runMiddlewaresStack({
        serviceName,
        router,
        rawLambdaEvent: record,
        rawLambdaContext,
        customMiddlewares
      });

      responses.push(response);
    } catch(error) {
      let errorResponse;

      if(!(error instanceof LambdaError)) {
        receivedUnhandledError = true;

        errorResponse = new ErrorResponse({
          eventId: record.eventID,
          error: error
        });
      } else {
        errorResponse = new ErrorResponse({
          eventId: record.eventID,
          error: error
        });
      }

      errors.push(errorResponse)
    }
  }

  const body = JSON.stringify({ errors: errors, data: responses });

  if(receivedUnhandledError) {
    const statusCode = 500;
    callback({ statusCode, body })
  } else {
    const statusCode = errors.length > 0 ? 400 : 200;
    callback(null, { statusCode, body })
  }
}
