import { parseBase64Data } from '../lib/utils';

import { BaseFactory, ConfigManager, IFactory, Logger, Metadata } from '..';
import { Context, Request, Response, Route } from '.'

const ARN_STREAM_NAME_REGEX_WITH_ENV = /\/(.*)-(.*)/;
const ARN_STREAM_NAME_REGEX_WITHOUT_ENV = /\/(.*)/;


export interface FactorySchema {
  serviceName: string;
  rawLambdaEvent: any;
  rawLambdaContext: any;
}

export class Factory extends BaseFactory implements Partial<IFactory> {
  private eventId: string;

  constructor(constructorAttributes: FactorySchema) {
    super(constructorAttributes)

    const data = parseBase64Data(this.rawLambdaEvent.kinesis.data);
    this.headers = data.headers || {};
    this.body = data.body;

    this.apiKey = this.headers.apiKey
    this.consolidatedRequestId = this.headers.consolidatedRequestId
    this.environment = this.headers.environment;
    this.lambdaRequestId = this.rawLambdaContext.awsRequestId;
    this.sourceServiceName = this.headers.sourceService

    const eventSourceARN = this.rawLambdaEvent.eventSourceARN;
    let streamNameRegExMatch = eventSourceARN.match(ARN_STREAM_NAME_REGEX_WITH_ENV) || eventSourceARN.match(ARN_STREAM_NAME_REGEX_WITHOUT_ENV);
    this.route = new Route({ streamName: streamNameRegExMatch[1] });

    this.eventId = this.rawLambdaEvent.eventID;
  }

  public createContext() : Context {
    const context = new Context({
      metadata: this.createMetadata(),
      request: this.createRequest(),
      response: this.createResponse(),
      logger: new Logger(this.createLoggingContext()),
      configManager: new ConfigManager({ serviceName: this.serviceName, environment: this.environment })
    });

    console.log(context);


    return context;
  }

  private createRequest() : Request {

    return new Request({
      route: this.route,
      headers: this.headers,
      body: this.body
    });
  }

  private createResponse() : Response {
    return new Response({ eventId: this.eventId });
  }
}
