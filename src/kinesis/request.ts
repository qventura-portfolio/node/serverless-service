import { BaseRequest } from '..';
import { Route } from '.';

export interface RequestSchema {
  body?: any;
  headers?: any;
  route: Route;
}

export class Request extends BaseRequest {
  constructor(constructorAttributes: RequestSchema) {
    super(constructorAttributes);
  }
}
