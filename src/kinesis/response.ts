import { BaseResponse, IResponse } from '..'


export interface ResponseSchema {
  eventId?: string;
  statusCode?: number;
  body?: any;
}

export class Response extends BaseResponse {
  public eventId: string;

  constructor(constructorAttributes: ResponseSchema = {}) {
    super();
    Object.assign(this, constructorAttributes);
  }
};
