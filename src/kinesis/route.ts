import { BaseRoute, IContext, IRoute } from '..';
import { Context } from '.';

export interface RouteSchema {
  streamName: string;
}

export class Route extends BaseRoute {
  public streamName: string;

  constructor(constructorAttributes: RouteSchema) {
    super();
    Object.assign(this, constructorAttributes);
  }

  equals(o: IRoute) {
    if(!(o instanceof Route)) {
      return false;
    }

    return this.streamName === o.streamName;
  };

  public toString() : string {
    return `Kinesis.Route { ${this.streamName} }`;
  }
};
