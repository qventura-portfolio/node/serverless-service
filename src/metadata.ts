export interface MetadataSchema {
  serviceName: string;
  environment: string;
  apiKey: string;
  consolidatedRequestId: string;
  lambdaRequestId: string;
  callerType?: string;
  callerId?: string;
  sourceServiceName? : string;
}

class Metadata {
  public serviceName: string;
  public environment: string;
  public apiKey: string;
  public consolidatedRequestId: string;
  public lambdaRequestId: string;
  public callerType: string = null;
  public callerId: string = null;
  public sourceServiceName: string = null;

  constructor(constructorAttributes: MetadataSchema) {
    Object.assign(this, constructorAttributes);
  }
}

export { Metadata }
