import * as Middlewares from './middlewares';
import { IMiddleware } from './middlewares/i-middleware';

import { composeMiddlewareStack } from './compose-middlewares-stack';
import { Factories, IResponse, Router } from '.';

export const runMiddlewaresStack = async (args : { serviceName: string, router: Router, rawLambdaEvent: any, rawLambdaContext: any, customMiddlewares: IMiddleware[] }) : Promise<IResponse> => {
  const { serviceName, router, rawLambdaEvent, rawLambdaContext, customMiddlewares } = args;
  const middlewareContext = Factories.getFactory({ serviceName, rawLambdaEvent, rawLambdaContext }).createContext();

  const middlewareStack = composeMiddlewareStack([
    Middlewares.logRequest,
    Middlewares.errorHandler,
    ...customMiddlewares,
    Middlewares.routeResolver(router)
  ])

  await middlewareStack(middlewareContext);

  return middlewareContext.response;
}
