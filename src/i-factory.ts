import { IContext, IRequest, IResponse, Metadata } from '.';

export interface IFactory {
  createContext(): IContext;
}
