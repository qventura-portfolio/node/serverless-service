import { IRoute } from '.';

export interface IRequest {
  route: IRoute;
  headers: any;
  body: any;

  beforeResolvingRoute(route: IRoute) : void;
}
