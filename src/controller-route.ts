import { IRoute } from '.';

export interface IControllerRoute {
  route: IRoute
  resolver: Function
}

export class ControllerRoute {
  public route: IRoute;
  public resolver: Function;

  constructor(constructorAttributes: IControllerRoute) {
    Object.assign(this, constructorAttributes);
  }
}
