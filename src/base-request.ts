import { IRequest, IRoute } from '.';

export interface BaseRequestSchema {
  route: IRoute;
  headers?: any;
  body?: any;
}

export abstract class BaseRequest implements Partial<IRequest> {
  public route: IRoute;
  public headers: any = {};
  public body: any = {};

  constructor(constructorAttributes: BaseRequestSchema) {
    Object.assign(this, constructorAttributes);
  }

  beforeResolvingRoute(route: IRoute) : void {
  };
}
