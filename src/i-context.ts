import { ConfigManager, Logger } from './lib';
import { IRoute, IRequest, IResponse, Metadata } from '.';

export interface IContext {
  logger: Logger;
  configManager: ConfigManager;
  metadata: Metadata;
  request: IRequest;
  response: IResponse;

  getLogger(): Logger;

  setResponse(statusCode: number, body: any) : void;
}
