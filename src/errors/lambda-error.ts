export class LambdaError extends Error {
  public statusCode: number = 500;
  public errorCode: number = 500;

  constructor({ errorMessage, statusCode, errorCode }) {
    super(errorMessage)

    this.statusCode = statusCode;
    this.errorCode = errorCode;
  }
}
