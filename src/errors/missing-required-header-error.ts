import { ErrorCodes, LambdaError } from '.';

export class MissingRequiredHeaderError extends LambdaError {
  constructor(public key: string) {
    super(ErrorCodes.MissingRequiredHeaderError(key));
  }
}
