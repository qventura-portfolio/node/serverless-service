import { ErrorCodes, LambdaError } from '.';

export class MissingRequiredContextAttributeError extends LambdaError {
  constructor(public key: string) {
    super(ErrorCodes.MissingRequiredContextAttributeError(key));
  }
}
