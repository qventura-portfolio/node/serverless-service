import { ErrorCodes, LambdaError } from '.';

export class RouteNotFoundError extends LambdaError {
  constructor(public routeArgs: any) {
    super(ErrorCodes.RouteNotFoundError(routeArgs));
  }
}
