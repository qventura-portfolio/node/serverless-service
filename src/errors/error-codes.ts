export const ErrorCodes = {
  MissingConfigurationError(region, key) {
    return {
      errorMessage: `Configuration not found in region '${region}' and for key ${key}`,
      statusCode: 500,
      errorCode: 1001
    }
  },
  MissingRequiredHeaderError(key) {
    return {
      errorMessage: `Required header '${key}' is missing`,
      statusCode: 400,
      errorCode: 1100
    }
  },
  MissingRequiredPathParameterError(key) {
    return {
      errorMessage: `Required path parameter '${key}' is missing`,
      statusCode: 400,
      errorCode: 1101
    }
  },
  MissingRequiredEventAttributeError(key) {
    return {
      errorMessage: `Required event attribute '${key}' is missing`,
      statusCode: 400,
      errorCode: 1102
    }
  },
  MissingRequiredContextAttributeError(key) {
    return {
      errorMessage: `Required context attribute '${key}' is missing`,
      statusCode: 400,
      errorCode: 1103
    }
  },
  RouteNotFoundError(route) {
    return {
      errorMessage: `No route matching ${JSON.stringify(route)}`,
      statusCode: 404,
      errorCode: 1104
    }
  },
  DocumentNotFoundError(klass: string, parameters : any = {}) {
    let message = `Cannot find document for class '${klass}'`;

    if(parameters) {
       message = message + ` searched with parameters: '${JSON.stringify(parameters)}'`;
    }

    return {
      errorMessage: message,
      statusCode: 404,
      errorCode: 1200
    }
  },
}
