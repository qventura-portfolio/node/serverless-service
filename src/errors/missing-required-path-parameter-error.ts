import { ErrorCodes, LambdaError } from '.';

export class MissingRequiredPathParameterError extends LambdaError {
  constructor(public key: string) {
    super(ErrorCodes.MissingRequiredPathParameterError(key));
  }
}
