import { ErrorCodes, LambdaError } from '.';

export class MissingConfigurationError extends LambdaError {
  constructor(public key: string) {
    super(ErrorCodes.MissingConfigurationError(process.env.AWS_REGION, key));
  }
}
