import { ErrorCodes, LambdaError } from '.';

export class DocumentNotFoundError extends LambdaError {
  constructor(public klass: string, public parameters: any = null) {
    super(ErrorCodes.DocumentNotFoundError(klass, parameters));
  }
}
