import { ErrorCodes, LambdaError } from '.';

export class MissingRequiredEventAttributeError extends LambdaError {
  constructor(public key: string) {
    super(ErrorCodes.MissingRequiredEventAttributeError(key));
  }
}
