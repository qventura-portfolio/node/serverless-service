import {
  IContext,
  IRequest,
  IResponse,
  IRoute,
  Metadata,
} from '.';

import {
  ConfigManager,
  Logger
} from './lib';

export interface BaseContextSchema {
  logger?: Logger;
  configManager?: ConfigManager;
  metadata?: Metadata;
  request?: IRequest;
  response?: IResponse;
}

export abstract class BaseContext implements Partial<IContext> {
  public logger: Logger = new Logger();
  public configManager: ConfigManager = new ConfigManager();
  public metadata: Metadata;
  public request: IRequest;
  public response: IResponse;

  constructor(constructorAttributes: BaseContextSchema = {}) {
    Object.assign(this, constructorAttributes);
  }

  public getLogger() : any {
    return this.logger;
  }

  public setResponse(statusCode: number, body: any) : void  {
    this.response.statusCode = statusCode;
    this.response.body = body;
  }
}
