import { IFactory, Metadata } from '.';

export interface FactorySchema {
  serviceName: string;
  rawLambdaEvent: any;
  rawLambdaContext: any;
}

export abstract class BaseFactory {
  public serviceName: string;
  public rawLambdaEvent: any;
  public rawLambdaContext: any;

  protected apiKey: string;
  protected body: any;
  protected consolidatedRequestId: string;
  protected environment: string;
  protected headers: any;
  protected lambdaRequestId: string;
  protected requestContext: any;
  protected route: any;
  protected sourceServiceName: string;

  constructor(constructorAttributes: FactorySchema) {
    this.serviceName = constructorAttributes.serviceName;
    this.rawLambdaEvent = constructorAttributes.rawLambdaEvent;
    this.rawLambdaContext = constructorAttributes.rawLambdaContext;
  }

  protected createMetadata() : Metadata {
    return new Metadata({
      serviceName: this.serviceName,
      environment: this.environment,
      apiKey: this.apiKey,
      consolidatedRequestId: this.consolidatedRequestId,
      lambdaRequestId: this.lambdaRequestId,
      sourceServiceName: this.sourceServiceName
    })
  }

  protected createLoggingContext() : string[]{
    return [
      this.serviceName,
      this.environment,
      this.apiKey,
      this.consolidatedRequestId,
      this.lambdaRequestId
    ]
  }
}
