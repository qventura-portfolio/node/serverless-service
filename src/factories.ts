import {
  Kinesis,
  IFactory,
  RestApi
} from '.';

export class Factories {
  public static getFactory(args: { serviceName: string, rawLambdaEvent: any, rawLambdaContext: any }) : IFactory {
    const { serviceName, rawLambdaEvent, rawLambdaContext } = args;

    if(Factories.isKinesisEvent(rawLambdaEvent)) {
      return new Kinesis.Factory({ serviceName, rawLambdaEvent, rawLambdaContext });
    } else {
      return new RestApi.Factory({ serviceName, rawLambdaEvent, rawLambdaContext });
    }
  }

  public static isKinesisEvent(rawLambdaEvent: any) : boolean {
    return rawLambdaEvent['eventName'] === 'aws:kinesis:record';
  }
}
