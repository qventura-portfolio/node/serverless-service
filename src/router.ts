import { ControllerRoute, IRoute } from '.';

export interface RouterSchema {
  controllerRoutes: ControllerRoute[];
}

export class Router {
  public controllerRoutes: ControllerRoute[] = [];

  constructor(constructorAttributes: RouterSchema) {
    Object.assign(this, constructorAttributes);
  }

  public getRouteMatching(route: IRoute) : ControllerRoute {
    return this.controllerRoutes.find((controllerRoute) => {
      return controllerRoute.route.equals(route)
    })
  }
}
