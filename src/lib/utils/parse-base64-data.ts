export const parseBase64Data = (base64Data) => {
  return JSON.parse(new Buffer(base64Data.toString(), 'base64').toString('ascii'));
}
