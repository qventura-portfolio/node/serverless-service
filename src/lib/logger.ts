import * as Winston from 'winston';

import { Metadata } from '..';


export class Logger extends Winston.Logger {
  constructor(logginContext: string[] = []) {
    super(Logger.baseConfiguration(logginContext));
  }

  public setContext(logginContext: string[]) {
    super.configure(Logger.baseConfiguration(logginContext));
  }

  static baseConfiguration(logginContext: string[]) {
    return {
      transports: [
        new (Winston.transports.Console)({
          timestamp: () => new Date().toISOString(),
          formatter: (options) => Logger.formatWithContext(logginContext, options)
        })
      ]
    }
  };

  public error (...args) {
    super.error(...args);
  }

  public info (...args) {
    super.info(...args);
  }

  public log (...args) {
    super.log(...args);
  }

  private static formatWithContext(logginContext: string[], options: any) {
    return `[${options.timestamp()}]${Logger.formatLogginContext(logginContext)}[${Logger.formatLevel(options)}][${Logger.sanitizedMessage(options)}][${JSON.stringify(options.meta)}]`
  }

  private static formatLogginContext(logginContext: string[]){
    if(logginContext.length == 0) {
      return '';
    }

    return `[${logginContext.join('][')}]`
  }

  private static sanitizedMessage(options) {
    return options.message.replace('\n', '\\n')
  }

  private static formatLevel(options) {
    return options.level.toUpperCase()[0];
  };
}
