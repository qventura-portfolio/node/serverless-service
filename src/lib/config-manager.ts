import * as AWS from 'aws-sdk';

import { MissingConfigurationError } from '../errors';
import { Metadata } from '..';
import { Logger } from '.';

const SSM = new AWS.SSM();

export interface ConfigManagerSchema {
  serviceName?: string;
  environment?: string;
  logger?: Logger;
}

export class ConfigManager {
  public environment : string;
  public serviceName : string;
  public logger: Logger = new Logger([this.serviceName, this.environment]);

  constructor(constructorAttributes: ConfigManagerSchema = {}) {
    Object.assign(this, constructorAttributes);
  }

  public setContext(args: { serviceName: string, environment: string }) : void {
    const { serviceName, environment } = args;

    if(serviceName !== undefined) {
      this.serviceName = serviceName;
    }

    if(environment !== undefined) {
      this.environment = environment;
    }
  }

  public async getEnvVar(key) : Promise<any> {
    const fullKey = `/${this.serviceName}/${this.environment}/${key}`;

    return this.get(fullKey);
  }

  public async getGlobalVariable(key) : Promise<any> {
    const fullKey = `/Global/${key}`;

    return this.get(fullKey);
  }

  public async getServiceUrl(key) : Promise<any> {
    const fullKey = `/ServiceDiscovery/ServiceUrls/${key}`;

    return this.get(fullKey);
  }

  public async getKinesisStreamName(key) : Promise<any> {
    const fullKey = `/ServiceDiscovery/KinesisStreams/${key}`;

    return this.get(fullKey);
  }

  public async get(key) : Promise<any> {
    if(this.logger) this.logger.info(`Requesting SSM parameter ${key}`);

    return new Promise((resolve, reject) => {
      SSM.getParameter({ Name: key }, (err, data) => {
        if(!err && data.Parameter.Value) {
          resolve(data.Parameter.Value);
        } else if(!err || err.name == 'ParameterNotFound') {
          if(this.logger) this.logger.error(`Missing configuration for ${key}`)

          reject(new MissingConfigurationError(key));
        } else {
          reject(err);
        }
      })
    });
  }
}
