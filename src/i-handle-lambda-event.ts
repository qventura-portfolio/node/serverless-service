import { Router } from '.';

export interface IHandleLambdaEvent {
  (args : { serviceName: string, router: Router, rawLambdaEvent: any, rawLambdaContext: any, callback: Function, customMiddlewares: Function[] }): Promise<void>;
}
