import * as pathToRegexp from 'path-to-regexp';

import { RouteNotFoundError } from '../errors'
import { IContext } from '..';
import { IMiddleware } from '.';

const routeResolver = (router) : IMiddleware => {
  return async (ctx: IContext, next: Function) : Promise<void> => {

    const controllerRoute = router.getRouteMatching(ctx.request.route);

    if(!controllerRoute) {
      throw new RouteNotFoundError(ctx.request.route);
    }

    ctx.request.beforeResolvingRoute(controllerRoute.route);

    await controllerRoute.resolver(ctx, next);
  }
}

export { routeResolver };
