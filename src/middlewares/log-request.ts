import { IContext } from '..';
import { IMiddleware } from '.';

const logRequest : IMiddleware = async (ctx: IContext, next: Function) : Promise<void> => {
  const startTime = new Date();
  const startTimeStr = startTime.toISOString();
  const route : any = ctx.request ? ctx.request.route : undefined;

  ctx.getLogger().info(`Request for route: ${route}, started at ${startTimeStr}`,
    {
      type: 'request_start',
      request: ctx.request,
      startTime: startTimeStr
    }
  );

  try {
    await next();
  } catch(error) {
    throw error;
  } finally {
    const endTime = new Date();
    const endTimeStr = endTime.toISOString();
    const duration = endTime.getTime() - startTime.getTime();

    ctx.getLogger().info(`Request for route: ${route}, completed with statusCode ${ctx.response.statusCode} after ${duration}ms, started at ${startTime}, ended at ${endTime}`,
      {
        type: 'request_end',
        request: ctx.request,
        startTime: startTimeStr,
        endTime: endTimeStr,
        duration: duration,
        response: ctx.response,
      }
    );
  }
}

export { logRequest };
