import { IContext } from '..';

export interface IMiddleware {
  (ctx: IContext, next: Function): Promise<void>;
}
