import { LambdaError } from '../errors';
import { IContext } from '..';
import { IMiddleware } from '.';

const errorHandler : IMiddleware =  async (ctx: IContext, next: Function) : Promise<void> => {
  try {
    await next();
  } catch (error) {
    if (error instanceof LambdaError) {
      ctx.response.statusCode = error.statusCode;
    }

    const errorBody = {
      error: {
        message: error.message,
        errorCode: error.errorCode,
        statusCode: error.statusCode,
      }
    }

    ctx.response.body = errorBody;

    ctx.getLogger().error(error.message, {
      ...errorBody,
      type: 'error',
      stacktrace: error.stack
    });

    throw error;
  }
};

export { errorHandler };
