import { IMiddleware } from './middlewares';
import { handleLambdaEvent as handleKinesisLambdaEvent } from './kinesis';
import { handleLambdaEvent as handleRestApiLambdaEvent } from './rest-api';
import { IHandleLambdaEvent, Router } from '.';

export const handleLambdaEvent = async (args : { serviceName: string, router: Router, rawLambdaEvent: any, rawLambdaContext: any, callback: Function, customMiddlewares: IMiddleware[] }) : Promise<void> => {
  const { serviceName, router, rawLambdaEvent, rawLambdaContext, callback, customMiddlewares } = args;

  rawLambdaContext.callbackWaitsForEmptyEventLoop = false;

  const lambdaHandler = getLambdaEventHandler(args.rawLambdaEvent);
  lambdaHandler({ serviceName, router, rawLambdaEvent, rawLambdaContext, callback, customMiddlewares });
}

const getLambdaEventHandler = (rawLambdaEvent: any) : IHandleLambdaEvent => {
  if(Array.isArray(rawLambdaEvent['Records'])) {
    return handleKinesisLambdaEvent;
  }

  return handleRestApiLambdaEvent;
}
