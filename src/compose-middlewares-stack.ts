import * as compose from 'koa-compose';

import { IMiddleware } from './middlewares';

export const composeMiddlewareStack = (middlewares : IMiddleware[]) : Function => {
  return compose(middlewares)
}
