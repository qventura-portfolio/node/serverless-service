import { IResponse } from '.';

export interface BaseResponseSchema {
  statusCode?: number;
  body?: any;
}

export abstract class BaseResponse implements Partial<IResponse> {
  public statusCode: number = 500;
  public body: any = {};

  constructor(constructorAttributes: BaseResponseSchema = {}) {
    Object.assign(this, constructorAttributes);
  }

  public setBody(body: any) : void {
    this.body = body;
  }
}
